'''
 * 	Project name: `MNIST AI`
 *	File name: `mnist.py`
 *	File Description: A neat and nice program that using tensorflow and keras makes an AI that can identify items from both MNIST and MNIST-Fashion
 *	
 *	
 *	
 *	Version log
 *	-----------
 *	Version 1 by Hexer, 20.7.2018
 *	Updates:
 * 		-Made the darn thing!
 *		-Added both mnist databses
 *		-Added testing local and database images
 *		-Added saving
 * 	Version 1.01 by PaperBag, 21.7.2018
 * 	Updates:
 * 		-Display a message guiding the user to install requirements
'''

from pathlib import Path
import os
import sys
import uuid

try:
	from tensorflow import keras
	from PIL import Image
	from keras.models import model_from_json
	import tensorflow as tf
	import numpy as np
except ImportError:
	sys.exit("Missing requirements. Please run `python -m pip install -r requirements.txt` first.")


fashion = input("\nIf you want to use the normal digits MNSIT database enter 'd' if you want to use the Fashion-MNSIT database enter 'f': ")
if fashion == "d":
	mnist = keras.datasets.mnist

	(train_images, train_labels), (test_images, test_labels) = mnist.load_data()

	class_names = ['0', '1', '2', '3', '4', 
				   '5', '6', '7', '8', '9']
elif fashion == "f":
	fashion_mnist = keras.datasets.fashion_mnist

	(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()

	class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat', 
               'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']
else:
	print("\n")
	sys.exit("Thats not 'd' or 'f'! Quitting program...\n")

gen = input("To generate a new session enter 'g' and to use an old session press 'u': ")

train_images = train_images / 255.0
test_imagesL = test_images
test_images = test_images / 255.0

if gen == 'g':
	print(">Doing computery stuff...\n")
	model = keras.Sequential([
		keras.layers.Flatten(input_shape=(28, 28)),
		keras.layers.Dense(16, activation=tf.nn.relu),
		keras.layers.Dense(16, activation=tf.nn.relu),
		keras.layers.Dense(16, activation=tf.nn.relu),
		keras.layers.Dense(10, activation=tf.nn.softmax)
	])
	print(">Model building finished")
	print("\n\n>Model summary:\n")
	s_summary = str(model.summary())
	print("\n\n")
	model.compile(optimizer=tf.train.AdamOptimizer(), 
				  loss='sparse_categorical_crossentropy',
				  metrics=['accuracy'])
	print(">Model compiling finished")

	ep = input("How many AI runs would you like: ")
	model.fit(train_images, train_labels, epochs=int(ep))
	print(">Model training finished\n")

	test_loss, test_acc = model.evaluate(test_images, test_labels)
	print(">Test evaluations finished\n")

	print('>\n\nTest loss = %.2f' % (test_loss*100))
	print('>Test accuracy = %.2f' % (test_acc*100), end='')
	print("%\n\n\n")
	print(">AI building completed\n\n")
	
	save = input(">Save session? (y/n): ")
	while save != 'y' and save != 'n':
		save = input(">Save session? (y/n): ")
	
	if save == 'y':
		uuid = str(uuid.uuid4())
		model_json = model.to_json()
		with open("models/"+uuid+"json.json", "w") as json_file:
			json_file.write(model_json)
		model.save_weights("models/"+uuid+"h5.h5")
		with open("models/"+uuid+"info.txt", "w") as txt:
			txt.write("UUID: "+uuid+"\n\n\nRuns: "+ep+"\n\nModel Summary:\n")
			model.summary(print_fn=lambda x: txt.write(x + '\n'))
			txt.write("\n\nTest Loss: "+str((test_loss*100))+"\nTest Accuracy: "+str((test_acc*100)))
		print(">Saved session as ", end='')
		print(uuid)
elif gen == 'u':
	uuid = input("Session uuid: ")
	if Path('models/'+uuid+'json.json').is_file() and Path('models/'+uuid+'h5.h5').is_file():
		json_file = open('models/'+uuid+'json.json', 'r')
		loaded_model_json = json_file.read()
		json_file.close()
		model = model_from_json(loaded_model_json)
		# load weights into new model
		model.load_weights('models/'+uuid+'h5.h5')
		print(">Model loading finished")
		
		print(">Model Info:")
		with open("models/"+uuid+"info.txt", "r") as txt:
			print("\n"+txt.read()+"\n\n")
		print("\n\n")
		print(">AI building completed\n\n")
	else:
		sys.exit("Session uuid isn't real! Qutting program!")
else:
	sys.exit("Thats not 'u' or 'g'! Quitting program...\n")

print(">Entering user testing mode...\n")
while True:
	test = input("Enter 't' to use test images and enter 'l' to use local images: \n")
	if test == "t":
		predictions = model.predict(test_images)
		print("Enter the letter 's' to switch types and 'q' to quit.")
		print("**Image numbers are from 0 to 9999, inputting more would crash the program.\n")
		while True:
			imgnum = input("Image number: ")
			if imgnum == 's':
				break
			if imgnum == 'q':
				sys.exit("\n\n\nGoodbye! Hope you had fun with my AI!\n")
			print("Prediction of image number " + imgnum + ": Is that a " + class_names[np.argmax(predictions[int(imgnum)])] + "?")
			print("Actual picture's content: " + class_names[test_labels[int(imgnum)]])
			result = Image.fromarray(test_imagesL[int(imgnum)], 'L')
			result.show()
			#result.save(imgnum+'.png')
			print("\n\n")
	elif test == "l":
		print("Enter the letter 's' to switch types and 'q' to quit.")
		print("**Your image has to be 28 by 28 in size \n")
		while True:
			imgname = input("Enter your image file path: ")
			if imgname == 's':
				break
			if imgname == 'q':
				sys.exit("\n\n\nGoodbye! Hope you had fun with my AI!\n")
			path = Path(imgname)
			if path.is_file():
				img = Image.open(imgname).convert("L")
				img.load()
				data = np.array(img) 
				#Image.fromarray(data, 'L').show()
				predata = np.array([data]);
				predictions = model.predict(predata)
				print("Prediction of file `" + imgname + "`: Is that a " + class_names[np.argmax(predictions[0])] + "?")
				print("\n\n")
			else:
				print("\nThat wasn't a real file, please try again.\n")
	else:
		print("That wasn't a 't' or an 'l', please try again.")